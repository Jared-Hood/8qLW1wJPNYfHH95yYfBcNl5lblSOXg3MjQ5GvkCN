<?php

namespace Drupal\pega_yext_support_center\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\node\NodeInterface;

/**
 * Create new views_count field for Question, Discussion, Idea Content Types.
 */
class LastActivity extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Computes the field value.
   */
  protected function computeValue() {
    $last_activity = NULL;
    $node = $this->getEntity();
    if ($node instanceof NodeInterface) {
      $updated_node = $node->getChangedTime();
      if ($node->hasField('field_reply') &&
        !$node->get('field_reply')->isEmpty() &&
        $node->field_reply->last_comment_timestamp > 0) {
        $updated_reply = $node->field_reply->last_comment_timestamp;
        // Overwrite last activity when we have comments.
        $value = ($updated_node > $updated_reply) ? $updated_node : $updated_reply;
        $last_activity = \Drupal::service('date.formatter')->formatTimeDiffSince($value);
      }
    }
    $this->list[0] = $this->createItem(0, $last_activity);
  }

}
