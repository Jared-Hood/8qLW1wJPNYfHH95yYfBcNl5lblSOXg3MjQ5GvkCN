<?php

namespace Drupal\pega_yext_support_center\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\node\NodeInterface;

/**
 * Create new views_count field for Question, Discussion, Idea Content Types.
 */
class NodesViewsCount extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Computes the field value.
   */
  protected function computeValue() {
    $node_views_count = 0;
    $node = $this->getEntity();
    if ($node instanceof NodeInterface) {
      $result = \Drupal::service('statistics.storage.node')->fetchView($node->id());
      if ($result) {
        $node_views_count = $result->getTotalCount();
      }
    }
    $this->list[0] = $this->createItem(0, $node_views_count);
  }

}
