<?php

namespace Drupal\pega_yext_academy\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\node\NodeInterface;
use Drupal\pega_content_stats\Plugin\PegaContentStats\Duration\DurationTrait;

/**
 * Create new completion_time field for Mission, Module, Challenge.
 *
 * Content Types.
 */
class CompletionTime extends FieldItemList {

  use ComputedItemListTrait;
  use DurationTrait;

  /**
   * Computes the field value.
   */
  protected function computeValue() {
    $duration = '';
    $node = $this->getEntity();
    if ($node instanceof NodeInterface) {
      $duration = \Drupal::service('pega_content_stats.duration')->getDurationTotal($node);
      $duration = $this->format($duration);
    }
    $this->list[0] = $this->createItem(0, $duration);
  }

}
