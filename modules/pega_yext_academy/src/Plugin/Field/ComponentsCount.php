<?php

namespace Drupal\pega_yext_academy\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\node\NodeInterface;

/**
 * Create new components_count field for Mission, Module, Challenge.
 *
 * Content Types.
 */
class ComponentsCount extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Computes the field value.
   */
  protected function computeValue() {
    $components_count = '';
    $node = $this->getEntity();
    if ($node instanceof NodeInterface) {
      $children = \Drupal::service('pega_content.referencing_entity')->getReferencedEntitiesRecursive($node, [
        'field_mission_steps', 'field_topics', 'field_content_steps',
      ], 1);
      $component_count = [];
      $pluginManager = \Drupal::service('plugin.manager.pega_content_stats');
      $count_tasks = $pluginManager->createInstance('pega_content_stats_count_tasks');
      if (!empty($children)) {
        foreach ($children as $child) {
          $bundle_label = $child->type->entity->label();
          if (!empty($bundle_label)) {
            $count = (isset($component_count[$bundle_label])) ? $component_count[$bundle_label] : 0;
            $count++;
            $component_count[$bundle_label] = $count;
          }
        }
      }
      foreach ($component_count as $component => $count) {
        // Challenge components labels are not adequate,
        // hence using the CountTasks plugin.
        if ($node->type->entity->label() == 'Challenge' && $labels = $count_tasks->getLabels()) {
          $component = $labels['section_text']['singular']->render();
        }
        $components_value = $this->formatPlural($count, "1 @content_count_label_singular", "@count @content_count_label_plural", [
          '@content_count_label_singular' => $component,
          '@content_count_label_plural' => $component . 's',
        ]);
        $components_count = $components_count . $components_value . ', ';
      }
    }
    $this->list[0] = $this->createItem(0, substr(trim($components_count), 0, -1));
  }

}
