<?php

namespace Drupal\pega_yext_community\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\node\NodeInterface;

/**
 * Create new reading_time field for Blog Content Type.
 */
class BlogReadingTime extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Computes the field value.
   */
  protected function computeValue() {
    $reading_time = 0;
    $blog_node = $this->getEntity();
    if ($blog_node instanceof NodeInterface) {
      $this->readingTime = \Drupal::service('node_read_time.reading_time');
      $reading_time_value = $this->readingTime
        ->setWordsPerMinute(256)
        ->collectWords($blog_node)
        ->calculateReadingTime()
        ->getReadingTime();
      // Clear the words variable.
      $this->readingTime->setWords(0);
      $reading_time = $this->t(
        '@time read', ['@time' => str_replace('minutes', 'minute', $reading_time_value)]
      );
    }
    $this->list[0] = $this->createItem(0, $reading_time);
  }

}
