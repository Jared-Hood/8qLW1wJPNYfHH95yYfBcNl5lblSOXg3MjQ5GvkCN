import { iconDocumentation } from '@bolt/elements-icon/src/icons/js/documentation';
import customCardTemplate from './standard-card-override.hbs';
import customHeaderTemplate from './results-section-header.hbs';
import './index.scss';
import { formatDate, formatChips } from './render-utils';

const createDataMappings = (ANSWERS, verticalKey) => {
  // Initial data mappings
  let dataMappings = {
    title: item => item.name,
    url: item => item.landingPageUrl,
    eyebrow: item => [item.c_contentType],
    details: item => item.richTextDescription,
    date: item => item.externalArticleUpdateDate,
    version: item => item.c_productVersion,
    uid: item => item.uid,
    showMoreLimit: 250,
  };

  // Modify data mappings by verticalKey
  switch (verticalKey) {
    case 'pegacommunity_articles':
      dataMappings = {
        ...dataMappings,
        eyebrow: item => ['Documentation', 'Article'],
        metaItems: item => {
          const items = [];

          if (item.c_bookName) {
            const bookData = {
              title: item.c_bookName,
              icon: iconDocumentation(),
            };

            if (item.c_bookUrl) {
              bookData['url'] = item.c_bookUrl;
            }

            items.push(bookData);
          }

          if (item.c_productVersion) {
            items.push({
              title: 'Choose a version',
              url: '#',
              children: [
                {
                  title: 'Version ' + item.c_productVersion,
                  url: item.landingPageUrl,
                },
                {
                  title: 'Previous two versions',
                  url: `https://docs-previous.pega.com/search?q=${item.name}`,
                },
              ],
            });
          }

          return items;
        },
        chips: item => formatChips(item, ANSWERS.components),
      };

      break;
    case 'blogs':
      dataMappings = {
        ...dataMappings,
        eyebrow: ['Community Blog'],
        metaItems: item => {
          const items = [];

          if (item.c_blogAuthor) {
            items.push({ title: item.c_blogAuthor });
          }

          if (item.c_readingTime) {
            items.push({ title: item.c_readingTime });
          }

          return items;
        },
        chips: item => formatChips(item, ANSWERS.components),
      };

      break;
    case 'marketplace':
      dataMappings = {
        ...dataMappings,
        eyebrow: ['Marketplace'],
        chips: item => formatChips(item, ANSWERS.components),
      };

      break;
    case 'training':
      dataMappings = {
        ...dataMappings,
        eyebrow: item => {
          return ['Pega Academy', item.c_contentType];
        },
        metaItems: item => {
          const metadata = [];
          const components = item.c_componentsCount
            ? item.c_componentsCount.split(',')
            : [];
          components.forEach(function (element) {
            metadata.push({ title: element });
          });
          metadata.push({ title: item.c_completionTime });

          return metadata;
        },
        chips: item => formatChips(item, ANSWERS.components),
      };

      break;
    case 'videos':
      dataMappings = {
        ...dataMappings,
        eyebrow: ['Video'],
        metaItems: item => {
          const items = [];

          if (item.c_videoType) {
            items.push({
              title: item.c_videoType,
            });
          }

          if (item.externalArticleUpdateDate) {
            items.push({
              title: formatDate(item.externalArticleUpdateDate),
            });
          }

          if (item.c_duration) {
            items.push({
              title: item.c_duration,
            });
          }

          return items;
        },
        chips: item => formatChips(item, ANSWERS.components),
        videoId: item => item.c_videoID,
        videoAccount: '1519050010001',
        videoPlayer: 'default',
        videoSignifier: item =>
          `<img src="${item.c_videoThumbnail}" alt="${item.c_videoAltText}" />`,
      };

      break;
    case 'support':
      dataMappings = {
        ...dataMappings,
        eyebrow: item => {
          return ['Support Center', item.c_contentType];
        },
        metaItems: item => {
          return [
            {
              title:
                'Posted by ' +
                item.c_postedBy +
                ' on ' +
                item.externalArticlePostDate,
            },
            {
              title: item.c_lastActivity
                ? 'Last activity: ' + item.c_lastActivity
                : '',
            },
          ];
        },
        chips: item => formatChips(item, ANSWERS.components),
        status: item => {
          return {
            solved: item.c_solved === '1',
            locked: item.c_replyStatus === '1',
            numberItems: [
              {
                number: item.c_replyCount ? item.c_replyCount : 0,
                label: 'Replies',
              },
              {
                number: item.c_viewsCount ? item.c_viewsCount : 0,
                label: 'Views',
              },
            ],
          };
        },
      };

      break;
    case 'knowledge_panel':
      dataMappings = {
        ...dataMappings,
        variant: 'card',
        subtitle: item => item.c_contentType,
        details: item =>
          `<div>${item.richTextDescription}</div><div><b>Related searches</b><br>${item.c_relatedTerms}</div>`,
      };

      break;
  }

  return dataMappings;
};

const initVerticalSearch = opts => {
  const {
    apiKey,
    blogsTabActive,
    businessId,
    docsTabActive,
    experienceKey,
    experienceVersion,
    marketplaceTabActive,
    supportTabActive,
    tabAllUrl,
    tabBlogsUrl,
    tabDocsUrl,
    tabKnowledgePanelUrl,
    tabMarketplaceUrl,
    tabSupportUrl,
    tabTrainingUrl,
    tabVideosUrl,
    trainingTabActive,
    verticalKey,
    videosTabActive,
  } = opts;

  ANSWERS.init({
    apiKey,
    experienceKey,
    businessId,
    experienceVersion,
    verticalKey,
    useTemplates: true,
    // templateBundle: TemplateBundle,
    search: {
      verticalKey,
      limit: 10,
      defaultInitialSearch: '',
    },
    verticalPages: [
      {
        label: 'All',
        url: tabAllUrl,
        isFirst: true,
      },
      {
        label: 'Documentation',
        url: tabDocsUrl,
        verticalKey: 'pegacommunity_articles',
        isActive: docsTabActive,
      },
      {
        label: 'Blogs',
        url: tabBlogsUrl,
        verticalKey: 'blogs',
        isActive: blogsTabActive,
      },
      {
        label: 'Training',
        url: tabTrainingUrl,
        verticalKey: 'training',
        isActive: trainingTabActive,
      },
      {
        label: 'Videos',
        url: tabVideosUrl,
        verticalKey: 'videos',
        isActive: videosTabActive,
      },
      {
        label: 'Support',
        url: tabSupportUrl,
        verticalKey: 'support',
        isActive: supportTabActive,
      },
      {
        label: 'Marketplace',
        url: tabMarketplaceUrl,
        verticalKey: 'marketplace',
        isActive: marketplaceTabActive,
      },
    ],
    onReady() {
      const ANSWERS = this;
      let callsToAction = [];

      // Trigger the vertical results to re-render once the facets have been loaded. When the
      // results are initially loaded we don't have any facet info to render the chips.
      ANSWERS.core.storage.registerListener({
        eventType: 'update',
        storageKey: 'facets-loaded',
        callback: () => {
          ANSWERS.components
            .getActiveComponent('VerticalResults')
            ._state.emit('update');
        },
      });

      // init components
      this.addComponent('SearchBar', {
        container: '.searchbar-container',
        allowEmptySearch: true,
        customIconUrl:
          window.location.origin +
          '/modules/shared/pega_yext/templates/partials/pega-search-icon.svg',
        verticalKey,
      });
      this.addComponent('Navigation', {
        container: '.navigation-container',
      });
      this.addComponent('Facets', {
        container: '.facets-container',
        verticalKey,
        searchOnChange: true,
        expand: false,
        // template: filters/facets
        transformFacets: (facets, config) => {
          // Configuration of individual facets.
          facets.map(facet => {
            switch (facet.fieldId) {
              case 'c_appComponentCategory':
              case 'c_videoType':
              case 'c_contentType':
                facet.sortOrder = 0;
                facet.displayName = 'Content Type';
                break;
              case 'c_parentTerm':
                facet.sortOrder = 1;
                facet.searchable = true;
                if (verticalKey === 'marketplace') {
                  facet.displayName = 'Compatible With';
                }
                break;
              case 'c_productVersion':
                facet.sortOrder = 2;
                facet.searchable = true;
                // Sort "Product Versions" in descending order.
                facet.options.sort((a, b) =>
                  b.displayName > a.displayName
                    ? 1
                    : a.displayName > b.displayName
                    ? -1
                    : 0,
                );
                break;
              case 'c_capability':
                facet.sortOrder = 3;
                facet.searchable = true;
                break;
              case 'c_industry':
                facet.sortOrder = 4;
                facet.searchable = true;
                break;
              case 'c_role':
                facet.sortOrder = 5;
                facet.searchable = true;
                break;
              case 'c_featuredListing':
                facet.sortOrder = 5;
                // Remove option "0" from "Featured Listing" facet (this is risky, as we assume it's always first on the list).
                facet.options.splice(0, 1);
                break;
              case 'c_pegaReviewed':
                facet.sortOrder = 5;
                facet.displayName = 'Security Status';
                // Remove option "0" from "Security Status" facet (this is risky, as we assume it's always first on the list).
                facet.options.splice(0, 1);
                break;
              case 'c_partner':
                facet.sortOrder = 6;
                facet.searchable = true;
                facet.showMoreLimit = 1;
                facet.showMoreLabel = 'Expand';
                facet.showLessLabel = 'Collapse';
                facet.placeholderText = 'Choose Partner';
                break;
              default:
                facet.sortOrder = 20;
                break;
            }
          });
          // Sort facets by "facet.sortOrder" property.
          facets.sort((a, b) =>
            a.sortOrder > b.sortOrder ? 1 : b.sortOrder > a.sortOrder ? -1 : 0,
          );
          return facets.map(facet => {
            return Object.assign({}, facet, {});
          });
        },
      });
      this.addComponent('SpellCheck', {
        container: '.spellcheck-container',
      });

      // Override StandardCard template
      this.registerTemplate('cards/standard', customCardTemplate);
      this.registerTemplate('results/verticalresults', customHeaderTemplate);

      this.addComponent('VerticalResults', {
        container: '.vertical-container',
        verticalKey,
        card: {
          cardType: 'StandardCard',
          dataMappings: createDataMappings(ANSWERS, verticalKey),
          callsToAction,
        },
        appliedFilters: {
          show: true,
          removable: true,
        },
        noResults: {
          displayAllResults: true,
        },
        onMount: () => {
          // need to call brightcove init after react is rendered
          // https://gitlab.com/pegadigital/bolt/pega_bolt/-/blob/8.x-5.x/features/pega_bolt_video/js/brightcove-integration.js
          var domFragment = document.getElementsByClassName(
            'yxt-Results--videos',
          )[0];
          if (domFragment != null) {
            Drupal.behaviors.pegaBrightcoveIntegration?.attach(
              domFragment,
              drupalSettings,
            );
          }
        },
      });
      this.addComponent('Pagination', {
        container: '.pagination-container',
        showFirstAndLastButton: true,
        pageLabel: '',
        maxVisiblePagesDesktop: 10,
        maxVisiblePagesMobile: 3,
        pinFirstAndLastPage: false,
        // template: results/pagination
        noResults: {
          visible: true,
        },
      });
    },
  });
};

const initUniversalSearch = opts => {
  const {
    apiKey,
    businessId,
    experienceKey,
    experienceVersion,
    tabAllUrl,
    tabBlogsUrl,
    tabDocsUrl,
    tabKnowledgePanelUrl,
    tabMarketplaceUrl,
    tabSupportUrl,
    tabTrainingUrl,
    tabVideosUrl,
    supportTabActive,
    marketplaceTabActive,
    videosTabActive,
    trainingTabActive,
    blogsTabActive,
    docsTabActive,
  } = opts;

  ANSWERS.init({
    apiKey,
    experienceKey,
    businessId,
    experienceVersion,
    useTemplates: true,
    // templateBundle: TemplateBundle,
    search: {
      limit: 20,
      defaultInitialSearch: '',
      universalLimit: {
        pegacommunity_articles: 5,
        training: 5,
        videos: 5,
        support: 5,
        marketplace: 5,
        knowledge_panel: 1,
      },
    },
    verticalPages: [
      {
        label: 'All',
        url: tabAllUrl,
        isFirst: true,
        verticalKey: 'all',
        isActive: 1,
      },
      {
        label: 'Documentation',
        url: tabDocsUrl,
        verticalKey: 'pegacommunity_articles',
        isActive: docsTabActive,
      },
      {
        label: 'Blogs',
        url: tabBlogsUrl,
        verticalKey: 'blogs',
        isActive: blogsTabActive,
      },
      {
        label: 'Training',
        url: tabTrainingUrl,
        verticalKey: 'training',
        isActive: trainingTabActive,
      },
      {
        label: 'Videos',
        url: tabVideosUrl,
        verticalKey: 'videos',
        isActive: videosTabActive,
      },
      {
        label: 'Support',
        url: tabSupportUrl,
        verticalKey: 'support',
        isActive: supportTabActive,
      },
      {
        label: 'Marketplace',
        url: tabMarketplaceUrl,
        verticalKey: 'marketplace',
        isActive: marketplaceTabActive,
      },
    ],
    onReady() {
      // init components
      this.addComponent('SearchBar', {
        container: '.searchbar-container',
        allowEmptySearch: true,
        customIconUrl:
          window.location.origin +
          '/modules/shared/pega_yext/templates/partials/pega-search-icon.svg',
      });
      this.addComponent('Navigation', {
        container: '.navigation-container',
      });

      this.addComponent('SpellCheck', {
        container: '.spellcheck-container',
      });

      let noResultsDetails = {
        template: '<div><em>No results found!</em></div>',
        displayAllResults: false,
      };
      // Override StandardCard template
      this.registerTemplate('cards/standard', customCardTemplate);
      this.registerTemplate('results/verticalresults', customHeaderTemplate);

      this.addComponent('UniversalResults', {
        container: '.universal-results-container',
        config: {
          pegacommunity_articles: {
            title: 'Documentation',
            icon: 'star',
            url: tabDocsUrl,
            viewMore: true,
            viewMoreLabel: 'View More',
            showResultCount: true,
            card: {
              cardType: 'Standard',
              dataMappings: createDataMappings(this, 'pegacommunity_articles'),
              callsToAction: () => [],
            },
            appliedFilters: {
              show: true,
              removable: true,
            },
            noResults: noResultsDetails,
          },
          blogs: {
            title: 'Blogs',
            icon: 'star',
            url: tabBlogsUrl,
            viewMore: true,
            viewMoreLabel: 'View More',
            showResultCount: true,
            card: {
              cardType: 'Standard',
              dataMappings: createDataMappings(this, 'blogs'),
              callsToAction: () => [],
            },
            appliedFilters: {
              show: true,
              removable: true,
            },
            noResults: noResultsDetails,
          },
          training: {
            title: 'Training',
            icon: 'star',
            url: tabTrainingUrl,
            viewMore: true,
            viewMoreLabel: 'View More',
            showResultCount: true,
            card: {
              cardType: 'Standard',
              dataMappings: createDataMappings(this, 'training'),
              callsToAction: () => [],
            },
            appliedFilters: {
              show: true,
              removable: true,
            },
            noResults: noResultsDetails,
          },
          videos: {
            title: 'Videos',
            icon: 'star',
            url: tabVideosUrl,
            viewMore: true,
            viewMoreLabel: 'View More',
            showResultCount: true,
            card: {
              cardType: 'Standard',
              dataMappings: createDataMappings(this, 'videos'),
              callsToAction: () => [],
            },
            appliedFilters: {
              show: true,
              removable: true,
            },
            noResults: noResultsDetails,
          },
          support: {
            title: 'Support',
            icon: 'star',
            url: tabSupportUrl,
            viewMore: true,
            viewMoreLabel: 'View More',
            showResultCount: true,
            card: {
              cardType: 'Standard',
              dataMappings: createDataMappings(this, 'support'),
              callsToAction: () => [],
            },
            appliedFilters: {
              show: true,
              removable: true,
            },
            noResults: noResultsDetails,
          },
          marketplace: {
            title: 'Marketplace',
            icon: 'star',
            url: tabMarketplaceUrl,
            viewMore: true,
            viewMoreLabel: 'View More',
            showResultCount: true,
            card: {
              cardType: 'Standard',
              dataMappings: createDataMappings(this, 'marketplace'),
              callsToAction: () => [],
            },
            appliedFilters: {
              show: true,
              removable: true,
            },
            noResults: noResultsDetails,
          },
          knowledge_panel: {
            title: 'Knowledge Panel',
            url: tabKnowledgePanelUrl,
            viewMore: false,
            showResultCount: true,
            card: {
              cardType: 'Standard',
              dataMappings: createDataMappings(this, 'knowledge_panel'),
            },
            appliedFilters: {
              show: true,
              removable: true,
            },
            noResults: noResultsDetails,
          },
        },
        onMount: () => {
          // need to call brightcove init after react is rendered
          // https://gitlab.com/pegadigital/bolt/pega_bolt/-/blob/8.x-5.x/features/pega_bolt_video/js/brightcove-integration.js
          var domFragment = document.getElementsByClassName(
            'yxt-Results--videos',
          )[0];
          if (domFragment != null) {
            Drupal.behaviors.pegaBrightcoveIntegration?.attach(
              domFragment,
              drupalSettings,
            );
          }
        },
      });
    },
  });
};

window.pega_yext = {
  initVerticalSearch,
  initUniversalSearch,
};
