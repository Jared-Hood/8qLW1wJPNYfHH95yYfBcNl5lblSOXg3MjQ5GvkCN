function generateFacetUrl(fieldName, value) {
  const url = new URL(window.location.href);
  let facetFilters = {};
  try {
    facetFilters = JSON.parse(url.searchParams.get('facetFilters'));
  } catch (e) {}

  if (facetFilters === null) {
    facetFilters = {};
  }

  let isSet = false;
  if (facetFilters[fieldName]) {
    facetFilters[fieldName].forEach(item => {
      if (item[fieldName]['$eq'] === value) {
        isSet = true;
      }
    });
  } else {
    facetFilters[fieldName] = [];
  }

  if (!isSet) {
    const filter = {};
    filter[fieldName] = { $eq: value };
    facetFilters[fieldName].push(filter);
  }

  url.searchParams.set('facetFilters', JSON.stringify(facetFilters));

  return url.toString();
}

export function formatDate(date) {
  const gmtDate = new Date(date),
    localizedDate = new Date(
      gmtDate.getTime() + Math.abs(gmtDate.getTimezoneOffset() * 60000),
    );

  return localizedDate.toLocaleDateString('en-us', {
    weekday: 'long',
    year: 'numeric',
    month: 'short',
    day: 'numeric',
  });
}

export function formatChips(item, components) {
  const chips = [];
  components = components || ANSWERS.components;

  const Facets = components.getActiveComponent('Facets');
  if (Facets) {
    Facets.getState('filters').forEach(filter => {
      if (item[filter.fieldId]) {
        const options = filter.options.map(option => option.label),
          values = Array.isArray(item[filter.fieldId])
            ? item[filter.fieldId]
            : [item[filter.fieldId]];

        values.forEach(value => {
          if (options.includes(value)) {
            chips.push({
              title: value,
              url: generateFacetUrl(filter.fieldId, value),
            });
          }
        });
      }
    });
  }

  return chips;
}
