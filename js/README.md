# React App

## Command line instructions

```
# if nvm not available?
brew install nvm http://tecadmin.net/install-nvm-macos-with-homebrew/

# If not currently using Node v14.18.1
nvm install 14.18.1
nvm use

# Install npm dependencies
yarn setup

# For local Drupal development
yarn watch

# For production Drupal builds
yarn build

# For local static site development on http://localhost:8080/
yarn serve
```
